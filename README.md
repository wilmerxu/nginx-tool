# nginx-tool

#### 介绍
Delphi编写的nginx工具，可以在本地修改配置、重新加载nginx配置。

#### 安装教程
本工具为绿色软件，直接打开编译目录下的NginxTool.exe即可使用。

#### 使用说明
![输入图片说明](https://images.gitee.com/uploads/images/2020/1128/154333_daf16b61_5673492.png "屏幕截图.png")

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
