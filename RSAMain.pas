unit RSAMain;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ComCtrls, LbRSA, LbAsym;

type
  TForm1 = class(TForm)
    Label1: TLabel;
    MText: TMemo;
    Label2: TLabel;
    KeyText1: TEdit;
    Label3: TLabel;
    KeyText2: TEdit;
    Label4: TLabel;
    ModText: TEdit;
    Label5: TLabel;
    CText: TMemo;
    Label6: TLabel;
    PText: TMemo;
    Label7: TLabel;
    ComboBox1: TComboBox;
    Button1: TButton;
    Button2: TButton;
    StatusBar1: TStatusBar;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

function StrToHex(InStr:String):String;
var
  StrResult,Temp:String;
  i:Integer;
begin
  StrResult:='';Temp:='';
  for I := 0 to Length(InStr) - 1 do
    begin
      Temp := Format('%x', [Ord(InStr[I + 1])]);
      if Length(Temp) = 1 then Temp := '0' + Temp;
        StrResult := StrResult + Temp;
    end;
  Result:=StrResult;
end;

function HexToInt(Hex: String): Integer;
var
  I, Res: Integer;
  ch: Char;
begin
  Res := 0;
  for I := 0 to Length(Hex) - 1 do
  begin
    ch := Hex[I + 1];
    if (ch >= '0') and (ch <= '9') then
      Res := Res * 16 + Ord(ch) - Ord('0')
    else if (ch >= 'A') and (ch <= 'F') then
      Res := Res * 16 + Ord(ch) - Ord('A') + 10
    else if (ch >= 'a') and (ch <= 'f') then
      Res := Res * 16 + Ord(ch) - Ord('a') + 10
    else raise Exception.Create('Error: not a Hex String');
  end;
  Result := Res;
end;

function RSAEncryption(InStr,ModStr,KeyStr:String;KeyBit:TLbAsymKeySize):String;
var
  RSA:TLbRSA;
  TempResult:String;
begin
  TempResult:='';
  RSA:=TLbRSA.Create(nil);
  if KeyBit=aks128 then//128λ
    RSA.KeySize:=aks128;
  if KeyBit=aks256 then//256λ
    RSA.KeySize:=aks256;
  if KeyBit=aks512 then//512λ
    RSA.KeySize:=aks512;
  if KeyBit=aks768 then//768λ
    RSA.KeySize:=aks768;
  if KeyBit=aks1024 then//1024λ
    RSA.KeySize:=aks1024;
  RSA.PublicKey.ModulusAsString:=ModStr;
  RSA.PublicKey.ExponentAsString:=KeyStr;
  TempResult:= RSA.EncryptString(InStr);
  RSA.Free;
  Result:=StrToHex(TempResult);
end;

function RSADecryption(InStr,ModStr,KeyStr:String;PrivateKey:TLbAsymKeySize):String;
var
  Str, Temp: String;
  I: Integer;
  RSA:TLbRSA;
begin
  Str := '';
  for I := 0 to Length(InStr) div 2 - 1 do
  begin
    Temp := Copy(InStr, I * 2 + 1, 2);
    Str := Str + Chr(HexToInt(Temp));
  end;

  RSA:=TLbRSA.Create(nil);
  if PrivateKey=aks128 then//128λ
    RSA.KeySize := aks128;
  if PrivateKey=aks256 then//256λ
    RSA.KeySize := aks256;
  if PrivateKey=aks512 then//512λ
    RSA.KeySize := aks512;
  if PrivateKey=aks768 then//768λ
    RSA.KeySize := aks768;
  if PrivateKey=aks1024 then//1024λ
    RSA.KeySize := aks1024;
  RSA.PrivateKey.ModulusAsString:=ModStr;
  RSA.PrivateKey.ExponentAsString:=KeyStr;
  Result:=RSA.DecryptString(Str);
end;

procedure TForm1.FormCreate(Sender: TObject);
begin
  Combobox1.ItemIndex:=0;
end;

procedure TForm1.Button1Click(Sender: TObject);
begin
  Case Combobox1.ItemIndex of
    0:
      CText.Text:=RSAEncryption(MText.Text,ModText.Text,KeyText1.Text,aks128);
    1:
      CText.Text:=RSAEncryption(MText.Text,ModText.Text,KeyText1.Text,aks256);
    2:
      CText.Text:=RSAEncryption(MText.Text,ModText.Text,KeyText1.Text,aks512);
    3:
      CText.Text:=RSAEncryption(MText.Text,ModText.Text,KeyText1.Text,aks768);
    4:
      CText.Text:=RSAEncryption(MText.Text,ModText.Text,KeyText1.Text,aks1024);
  end;
end;

procedure TForm1.Button2Click(Sender: TObject);
begin
  Case Combobox1.ItemIndex of
    0:
      PText.Text:=RSADecryption(CText.Text,ModText.Text,KeyText2.Text,aks128);
    1:
      PText.Text:=RSADecryption(CText.Text,ModText.Text,KeyText2.Text,aks256);
    2:
      PText.Text:=RSADecryption(CText.Text,ModText.Text,KeyText2.Text,aks512);
    3:
      PText.Text:=RSADecryption(CText.Text,ModText.Text,KeyText2.Text,aks768);
    4:
      PText.Text:=RSADecryption(CText.Text,ModText.Text,KeyText2.Text,aks1024);
  end;
end;

end.
