object Form1: TForm1
  Left = 192
  Top = 114
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = 'RSA'#31639#27861#28436#31034
  ClientHeight = 412
  ClientWidth = 657
  Color = clBtnFace
  Font.Charset = GB2312_CHARSET
  Font.Color = clWindowText
  Font.Height = -12
  Font.Name = #23435#20307
  Font.Style = []
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 12
  object Label1: TLabel
    Left = 16
    Top = 16
    Width = 41
    Height = 12
    AutoSize = False
    Caption = #26126#25991#65306
  end
  object Label2: TLabel
    Left = 16
    Top = 83
    Width = 49
    Height = 12
    AutoSize = False
    Caption = #20844#38053#65306
  end
  object Label3: TLabel
    Left = 16
    Top = 115
    Width = 41
    Height = 12
    AutoSize = False
    Caption = #31169#38053#65306
  end
  object Label4: TLabel
    Left = 16
    Top = 147
    Width = 41
    Height = 12
    AutoSize = False
    Caption = #27169#25968#65306
  end
  object Label5: TLabel
    Left = 16
    Top = 176
    Width = 41
    Height = 12
    AutoSize = False
    Caption = #23494#25991#65306
  end
  object Label6: TLabel
    Left = 16
    Top = 240
    Width = 41
    Height = 12
    AutoSize = False
    Caption = #26126#25991#65306
  end
  object Label7: TLabel
    Left = 16
    Top = 339
    Width = 41
    Height = 12
    AutoSize = False
    Caption = #20301#25968#65306
  end
  object MText: TMemo
    Left = 16
    Top = 32
    Width = 625
    Height = 41
    TabOrder = 0
  end
  object KeyText1: TEdit
    Left = 48
    Top = 80
    Width = 593
    Height = 20
    TabOrder = 1
    Text = 'C52A'
  end
  object KeyText2: TEdit
    Left = 48
    Top = 112
    Width = 593
    Height = 20
    TabOrder = 2
    Text = '0D59BE2E5563469FA3A6FD1A218F7266'
  end
  object ModText: TEdit
    Left = 48
    Top = 144
    Width = 593
    Height = 20
    TabOrder = 3
    Text = '93A29081019B595865D608BE8C144885'
  end
  object CText: TMemo
    Left = 16
    Top = 192
    Width = 625
    Height = 41
    TabOrder = 4
  end
  object PText: TMemo
    Left = 16
    Top = 256
    Width = 625
    Height = 41
    TabOrder = 5
  end
  object ComboBox1: TComboBox
    Left = 48
    Top = 336
    Width = 145
    Height = 20
    ItemHeight = 12
    TabOrder = 6
    Items.Strings = (
      '128'#20301
      '256'#20301
      '512'#20301
      '768'#20301
      '1024'#20301)
  end
  object Button1: TButton
    Left = 320
    Top = 312
    Width = 153
    Height = 73
    Caption = #21152#23494
    TabOrder = 7
    OnClick = Button1Click
  end
  object Button2: TButton
    Left = 488
    Top = 312
    Width = 153
    Height = 73
    Caption = #35299#23494
    TabOrder = 8
    OnClick = Button2Click
  end
  object StatusBar1: TStatusBar
    Left = 0
    Top = 393
    Width = 657
    Height = 19
    Panels = <
      item
        Text = #12298#36719#20214#21152#35299#23494#25216#26415'-'#36719#20214#21152#23494#12299
        Width = 170
      end
      item
        Text = #20316#32773#65306#21490#23376#33635
        Width = 90
      end
      item
        Text = 'http://www.pefine.com'
        Width = 50
      end>
  end
end
